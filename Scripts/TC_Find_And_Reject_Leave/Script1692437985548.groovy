import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('TC_Login_Orange_HRM'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('for_Leave_List/menu_Leave'))

WebUI.verifyElementText(findTestObject('for_Leave_List/text_Leave'), 'Leave')

WebUI.verifyElementText(findTestObject('for_Leave_List/text_Leave List'), 'Leave List')

WebUI.verifyElementText(findTestObject('for_Leave_List/text_From Date'), 'From Date')

WebUI.verifyElementText(findTestObject('for_Leave_List/text_To Date'), 'To Date')

WebUI.verifyElementText(findTestObject('for_Leave_List/text_Leave Type'), 'Leave Type')

WebUI.verifyElementText(findTestObject('for_Leave_List/text_Show Leave with Status'), 'Show Leave with Status')

WebUI.sendKeys(findTestObject('for_Leave_List/textbox_From_Date'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('for_Leave_List/textbox_From_Date'), Keys.chord(Keys.DELETE))

WebUI.setText(findTestObject('for_Leave_List/textbox_From_Date'), '2022-01-01')

WebUI.clearText(findTestObject('for_Leave_List/textbox_To_Date'), FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('for_Leave_List/textbox_To_Date'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('for_Leave_List/textbox_To_Date'), Keys.chord(Keys.DELETE))

WebUI.setText(findTestObject('for_Leave_List/textbox_To_Date'), '2022-12-31')

WebUI.click(findTestObject('for_Leave_List/icon_(x)_Pending_Approval'))

WebUI.verifyElementText(findTestObject('for_Leave_List/text_Required'), 'Required')

WebUI.click(findTestObject('for_Leave_List/dropdown_Leave_Status'))

status_Pending_App = WebUI.getText(findTestObject('for_Leave_List/select_Pending Approval'))

println("$status_Pending_App")

WebUI.click(findTestObject('for_Leave_List/select_Pending Approval'))

WebUI.click(findTestObject('for_Leave_List/button_Search'))

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('for_Leave_List/button_Search'), 1)

// Verify The Result by the status
numberoftheday_1 = WebUI.getText(findTestObject('for_Leave_List/text_Number_Of_Days_Row1'))

println("$numberoftheday_1")

String Combine_Text = ((status_Pending_App + ' (') + numberoftheday_1) + ')'

println("$Combine_Text")

String Actual_Statu_User = WebUI.getText(findTestObject('for_Leave_List/text_Status_Row1'))

if (Actual_Statu_User == Combine_Text) {
    println("user found with Status: $Actual_Statu_User")
} else {
    throw new Exception('Test Failed')
}

User_name = WebUI.getText(findTestObject('for_Leave_List/text_Employee_Name_Row1'))

TestObject dateTestObject = findTestObject('for_Leave_List/text_Date_Row1')

Date_User_Leave1 = WebUI.getText(dateTestObject)

println("$Date_User_Leave1")

// Approve the leave
WebUI.click(findTestObject('for_Leave_List/button_Reject'))

// Nofication Pop Up
WebUI.verifyElementPresent(findTestObject('for_Leave_List/popup_SuccessSuccessfully Updated'), 1)

WebUI.verifyElementPresent(findTestObject('for_Leave_List/text_Success'), 1)

WebUI.verifyElementPresent(findTestObject('for_Leave_List/text_Successfully Updated'), 1)

// Make Sure that the status leave be Taken
WebUI.scrollToElement(findTestObject('for_Leave_List/text_Leave'), 1)

WebUI.scrollToPosition(999999, 0)

WebUI.click(findTestObject('for_Leave_List/icon_(x)_Pending_Approval'))

WebUI.click(findTestObject('for_Leave_List/dropdown_Leave_Status'))

Taken_status = WebUI.getText(findTestObject('for_Leave_List/span_Rejected'))

println("$Taken_status")

WebUI.click(findTestObject('for_Leave_List/select_Taken'))

WebUI.setText(findTestObject('for_Leave_List/textbox_Employee_Name'), User_name)

WebUI.click(findTestObject('for_Leave_List/select_The_Username'))

WebUI.click(findTestObject('for_Leave_List/button_Search'))

String username_after_acc = WebUI.getText(findTestObject('for_Leave_List/text_Employee_Name_Row1'))

println("$username_after_acc")

TestObject dateTestObject2 = findTestObject('for_Leave_List/text_Date_Row1')

Date_User_Leave2 = WebUI.getText(dateTestObject)

println("$Date_User_Leave2")

println("$Date_User_Leave2")

String number_of_day_Taken_Status = WebUI.getText(findTestObject('for_Leave_List/text_Number_Of_Days_Row1'))

println("$number_of_day_Taken_Status")

String actual_Status_Taken = WebUI.getText(findTestObject('for_Leave_List/text_Status_Row1'))

println("$actual_Status_Taken")

String Combine_Text2 = ((Taken_status + ' (') + number_of_day_Taken_Status) + ')'

println("$Combine_Text2")

String Actual_Statu_User1 = WebUI.getText(findTestObject('for_Leave_List/text_Status_Row1'))

if ((username_after_acc == User_name) && (Actual_Statu_User1 == Combine_Text2)) {
    println("user found with Status: $Actual_Statu_User")
} else {
    throw new Exception('Test Failed')
}

