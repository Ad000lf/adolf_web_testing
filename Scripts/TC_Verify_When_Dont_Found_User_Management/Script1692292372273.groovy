import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('TC_Login_Orange_HRM'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('for_Admin/menu_Admin'))

WebUI.verifyElementText(findTestObject('for_Admin/text_Admin'), 'Admin')

WebUI.verifyElementText(findTestObject('for_Admin/text_User Management'), 'User Management')

WebUI.verifyElementVisible(findTestObject('for_Admin/subMenu_User Management'))

WebUI.verifyElementVisible(findTestObject('for_Admin/subMenu_Job'))

WebUI.verifyElementVisible(findTestObject('for_Admin/subMenu_Organization'))

WebUI.verifyElementVisible(findTestObject('for_Admin/subMenu_Qualifications'))

WebUI.verifyElementVisible(findTestObject('for_Admin/subMenu_Nationalities'))

WebUI.verifyElementVisible(findTestObject('for_Admin/subMenu_Corporate Branding'))

WebUI.verifyElementVisible(findTestObject('for_Admin/subMenu_Configuration'))

WebUI.verifyElementText(findTestObject('for_Admin/text_System Users'), 'System Users')

WebUI.verifyElementText(findTestObject('for_Admin/text_Username'), 'Username')

WebUI.verifyElementText(findTestObject('for_Admin/text_User Role'), 'User Role')

WebUI.verifyElementText(findTestObject('for_Admin/text_Employee Name'), 'Employee Name')

WebUI.verifyElementText(findTestObject('for_Admin/text_Status'), 'Status')

WebUI.verifyElementText(findTestObject('for_Admin/text_User Role'), 'User Role')

WebUI.click(findTestObject('for_Admin/textbox_Username'))

WebUI.setText(findTestObject('for_Admin/textbox_Username'), 'CCC')

WebUI.click(findTestObject('for_Admin/button_Search'))

WebUI.verifyElementPresent(findTestObject('for_Admin/popup_InfoNo Records Found'), 1)

WebUI.verifyElementPresent(findTestObject('for_Admin/text_Info'), 1)

WebUI.verifyElementPresent(findTestObject('for_Admin/text_No Records Found'), 1)

WebUI.takeScreenshot()

WebUI.closeBrowser()

