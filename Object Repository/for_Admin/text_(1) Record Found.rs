<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_(1) Record Found</name>
   <tag></tag>
   <elementGuidId>87611f1b-737b-4035-a770-43d83d1f108e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '(1) Record Found' or . = '(1) Record Found')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/div[2]/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.orangehrm-horizontal-padding.orangehrm-vertical-padding > span.oxd-text.oxd-text--span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>278e604e-bbc8-46bf-94b0-a1d1a3fdfb68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>oxd-text oxd-text--span</value>
      <webElementGuid>7783b3fa-f913-4680-a273-57baca102ee7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>(1) Record Found</value>
      <webElementGuid>0e5df5c5-a7cc-42dc-86e4-86084cfe8f11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;oxd-layout&quot;]/div[@class=&quot;oxd-layout-container&quot;]/div[@class=&quot;oxd-layout-context&quot;]/div[@class=&quot;orangehrm-background-container&quot;]/div[@class=&quot;orangehrm-paper-container&quot;]/div[2]/div[@class=&quot;orangehrm-horizontal-padding orangehrm-vertical-padding&quot;]/span[@class=&quot;oxd-text oxd-text--span&quot;]</value>
      <webElementGuid>881c8f92-15e2-4238-a4cd-3d80de735e40</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div/span</value>
      <webElementGuid>ce5386cd-a356-4f4c-8887-e1170b7d3bbc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='(1) Record Found']/parent::*</value>
      <webElementGuid>351f6b78-f297-470c-b02a-633b3aab1c5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/span</value>
      <webElementGuid>1d63ac66-0b36-4e59-bb72-a1a7849d1f3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '(1) Record Found' or . = '(1) Record Found')]</value>
      <webElementGuid>76d00bb8-618a-4ca2-92b9-0fd09db7bf75</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
