<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>columntName_Status</name>
   <tag></tag>
   <elementGuidId>fc730569-e42e-4271-9a8e-a82b2cc78948</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[3]/div/div/div/div[5]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0bc1ae68-4262-49ba-bec0-f50a34d53dbc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>oxd-table-header-cell oxd-padding-cell oxd-table-th</value>
      <webElementGuid>9b9e3daf-8792-4dfe-a655-44ab8228c4a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>columnheader</value>
      <webElementGuid>03a73851-6999-4d4d-8832-d4624973bcee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>StatusAscendingDescending</value>
      <webElementGuid>a676bd05-6585-4a6a-909f-bd52888e61e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;oxd-layout&quot;]/div[@class=&quot;oxd-layout-container&quot;]/div[@class=&quot;oxd-layout-context&quot;]/div[@class=&quot;orangehrm-background-container&quot;]/div[@class=&quot;orangehrm-paper-container&quot;]/div[@class=&quot;orangehrm-container&quot;]/div[@class=&quot;oxd-table&quot;]/div[@class=&quot;oxd-table-header&quot;]/div[@class=&quot;oxd-table-row oxd-table-row--with-border&quot;]/div[@class=&quot;oxd-table-header-cell oxd-padding-cell oxd-table-th&quot;]</value>
      <webElementGuid>b92e8030-e347-46cb-ab48-eae5d6e484ed</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[2]/div[2]/div/div[2]/div[3]/div/div/div/div[5]</value>
      <webElementGuid>3f2580ca-2f98-41fe-9056-783fb6becb5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]</value>
      <webElementGuid>ca0480e5-ee81-4bfa-a285-3e24c6d04866</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'StatusAscendingDescending' or . = 'StatusAscendingDescending')]</value>
      <webElementGuid>7c1bd260-1a99-4d3f-ab90-be0ca9bf5222</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
