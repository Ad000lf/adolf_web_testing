<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Admin</name>
   <tag></tag>
   <elementGuidId>737f8ae7-c38e-4889-a8fb-11780e6d45b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h6[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h6.oxd-text.oxd-text--h6.oxd-topbar-header-breadcrumb-module</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h6</value>
      <webElementGuid>7ed2b8ee-e62c-45d0-9514-d26f177548f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>oxd-text oxd-text--h6 oxd-topbar-header-breadcrumb-module</value>
      <webElementGuid>9a4dbf3a-f848-4402-bbcd-e5d3e440f89c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Admin</value>
      <webElementGuid>429326f4-1bf0-41ac-a37b-5d56728a98e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;oxd-layout&quot;]/div[@class=&quot;oxd-layout-navigation&quot;]/header[@class=&quot;oxd-topbar&quot;]/div[@class=&quot;oxd-topbar-header&quot;]/div[@class=&quot;oxd-topbar-header-title&quot;]/span[@class=&quot;oxd-topbar-header-breadcrumb&quot;]/h6[@class=&quot;oxd-text oxd-text--h6 oxd-topbar-header-breadcrumb-module&quot;]</value>
      <webElementGuid>f8ca31ac-83e4-46b0-a510-b449ee4e8106</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/header/div/div/span/h6</value>
      <webElementGuid>0458c343-622a-4587-bc9d-af303fe66ad0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='User Management'])[1]/preceding::h6[2]</value>
      <webElementGuid>6aa4bdbf-5183-429d-8f30-c8e8943a9a6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h6</value>
      <webElementGuid>18da6243-36a1-46ec-a80a-0b04abab6d04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h6[(text() = 'Admin' or . = 'Admin')]</value>
      <webElementGuid>18bb6d01-43be-4afb-b108-c7c1dd4f7d16</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
