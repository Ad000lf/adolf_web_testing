<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Include Past Employees</name>
   <tag></tag>
   <elementGuidId>ec9825bc-7987-4728-acc8-1daac8d8f2ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.oxd-text.oxd-text--p.orangehrm-leave-filter-text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[2]/div[2]/div/div/div[2]/form/div[2]/div/div[3]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>c467b5dd-bf57-4131-8bec-3dca3972119a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>oxd-text oxd-text--p orangehrm-leave-filter-text</value>
      <webElementGuid>582c3992-a792-45f7-b3cb-7e69becf99c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Include Past Employees</value>
      <webElementGuid>f7ce8ed3-bf86-4714-8b19-5602c44035c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;oxd-layout&quot;]/div[@class=&quot;oxd-layout-container&quot;]/div[@class=&quot;oxd-layout-context&quot;]/div[@class=&quot;orangehrm-background-container&quot;]/div[@class=&quot;oxd-table-filter&quot;]/div[@class=&quot;oxd-table-filter-area&quot;]/form[@class=&quot;oxd-form&quot;]/div[@class=&quot;oxd-form-row&quot;]/div[@class=&quot;oxd-grid-4 orangehrm-full-width-grid&quot;]/div[@class=&quot;oxd-grid-item oxd-grid-item--gutters orangehrm-leave-filter --span-column-2&quot;]/p[@class=&quot;oxd-text oxd-text--p orangehrm-leave-filter-text&quot;]</value>
      <webElementGuid>33d3cccb-8343-42ca-bd4b-af96dff64615</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[2]/div[2]/div/div/div[2]/form/div[2]/div/div[3]/p</value>
      <webElementGuid>25fec6f3-1819-4f40-b77e-55351a3a2181</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-- Select --'])[2]/following::p[1]</value>
      <webElementGuid>e3f2fbe0-4556-45e1-8da6-361d17e061ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sub Unit'])[1]/following::p[1]</value>
      <webElementGuid>40a88604-2568-44e7-84a9-7c96dc368766</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Include Past Employees']/parent::*</value>
      <webElementGuid>dcf0bf9d-7b3e-4b6c-8793-f2989f10865d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[3]/p</value>
      <webElementGuid>7151580a-d155-437d-bec6-f3ce0ad47e54</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Include Past Employees' or . = 'Include Past Employees')]</value>
      <webElementGuid>56a5fcc6-882f-44c0-80c4-7285fb8201d4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
