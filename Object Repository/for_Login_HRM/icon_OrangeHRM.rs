<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>icon_OrangeHRM</name>
   <tag></tag>
   <elementGuidId>2edc0b35-f44e-449d-9d19-5cbff8a30ad5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>img[alt=&quot;company-branding&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[1]/div/div[1]/div[1]/div[1]/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>97d8d598-5732-4243-a073-bd002bb8d099</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/web/images/ohrm_branding.png?v=1689053487449</value>
      <webElementGuid>207fdb55-db5c-4c1e-9c9a-f8743068556b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>company-branding</value>
      <webElementGuid>807d0e07-6885-4da1-82a3-39bd5c22e8e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;orangehrm-login-layout&quot;]/div[@class=&quot;orangehrm-login-layout-blob&quot;]/div[@class=&quot;orangehrm-login-container&quot;]/div[@class=&quot;orangehrm-login-slot-wrapper&quot;]/div[@class=&quot;orangehrm-login-branding&quot;]/img[1]</value>
      <webElementGuid>e53017e9-9ce4-40d6-a447-5e307ce7888a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div/div/img</value>
      <webElementGuid>5550abb6-eb91-4535-ac5d-4a75de0f5422</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[@alt='company-branding']</value>
      <webElementGuid>84bd1f10-add5-472d-937d-a4c876cfae0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//img</value>
      <webElementGuid>95b91062-d8f5-4453-9ce8-e053bbaeffd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = '/web/images/ohrm_branding.png?v=1689053487449' and @alt = 'company-branding']</value>
      <webElementGuid>561b181a-20e2-4460-965f-5e1a5a468e70</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
